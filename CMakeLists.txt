#CMakeLists /crazy_race 

cmake_minimum_required(VERSION 2.8)
project(crazyrace)

set(LIBRARY_OUTPUT_PATH "${PROJECT_SOURCE_DIR}/lib")
set(EXECUTABLE_OUTPUT_PATH "${PROJECT_SOURCE_DIR}/bin")
set(SOURCES_PATH "${PROJECT_SOURCE_DIR}/src")
set(LIBRARY_PATH "${PROJECT_SOURCE_DIR}/src/libraries")

add_subdirectory (src)

