
#include "libraries/Mainfunctions.h"

using namespace std; 

/*! \file main.cpp
    \brief Main game loop.
*/

int  w2=900, h2=550; //!< Window width and height

//! Main game loop
int main(int argc, char ** argv) 
{
	
        initial();
        glutInit(&argc, argv);
        glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
        glutInitWindowPosition(200,50);
        glutInitWindowSize(w2,h2);
        glutCreateWindow("CRAZY RACE");

        glutDisplayFunc(juego);
        glutReshapeFunc(changeSize);
        glutIdleFunc(juego);
        
	glutMouseFunc(mouse);
	glutPassiveMotionFunc(mousePassive);      
	glutSpecialFunc(processSpecialKeys);
	glutKeyboardFunc(processNormalKeys);
        glutMainLoop();

        return 0;
}
