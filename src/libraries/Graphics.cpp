#include "Graphics.h"

Graphics::Graphics()
{
	despl=0.1f;
	this->screenx= 3.3f;
	this->screeny= 4.5f; 

}

float Graphics ::getScreenX()
{
	return screenx; 
}

float  Graphics ::getScreenY()
{
	return screeny; 
}


void Graphics ::textures (const char*  route, float alto, float ancho, float width, float height, float imgoff) // alto, ancho-->foto, width, height --> rectangulo base
{
		
		 
	//VARIABLES TEXTURA
                 GLuint imagen; //texture
                 
                 static unsigned char * data_tex;

   	 glEnable(GL_TEXTURE_2D); //Habilita Texturas 2D
             
		// Leemos la imagen

                         FILE * file;

			 file = fopen( route, "r" );  //MI ARCHIVO
	
			 if (file == NULL )
                                 {
                                     cerr<<"No carga textura "<<route<<endl;
                                 }

		 data_tex = (unsigned char *)malloc(ancho*alto*5);
                         fread( data_tex, ancho * alto * 3, 1, file );
                         fclose( file );		


	 // Cargamos la imagen de la textura

                         glGenTextures(1, &imagen);
                         glBindTexture(GL_TEXTURE_2D,imagen);

			glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

			glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);// GL_DECAL --> replace, GL_MODULATE --> mix

			 glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, alto, ancho, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, data_tex);
                         glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
                         glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
                         glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                         glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);


                         

		
			
	      glBegin( GL_QUADS );  //BEGIN AT TOP LEFT, CLOCKWISE (casi seguro xD)
                 glTexCoord2d(0.0 + imgoff ,1.0);   glVertex2f(-width/2,+height/2);  
                 glTexCoord2d(1.0 + imgoff ,1.0);   glVertex2f(+width/2,+height/2);
                 glTexCoord2d(1.0 + imgoff ,0.0);   glVertex2f(+width/2,-height/2);
                 glTexCoord2d(0.0 + imgoff ,0.0);   glVertex2f(-width/2,-height/2);
              glEnd();

		glFlush();

		glPopMatrix();  

		free(data_tex); // libero el espacio asignado a data_tex

		glDisable(GL_TEXTURE_2D);  //Desabilito Textura 2D


  glDeleteTextures( 1, &imagen );
}

float Graphics::getX()
{
	return xcoor; 
} 

float Graphics::getY()
{
	return ycoor;
} 


void Graphics::setX(float num)
{
	xcoor=num; 
} 

void Graphics::setY(float num)
{
	ycoor=num;
} 

void Graphics::printtext(float x, float y, char *word) {
  char *c;
  glRasterPos3f(x, y,0.0f);
  for (c=word; *c != '\0'; c++) {
    glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, *c);
  }
}


