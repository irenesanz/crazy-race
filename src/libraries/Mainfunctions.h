#ifndef MAIN_FUNCTIONS_H
#define MAIN_FUNCTIONS_H

#include "Player.h"
#include "F_Objects.h"
#include "Background.h"
#include "Music.h"
#include "Yoda.h"
#include "R2D2.h"
#include "Star.h"
#include "Foe.h"

#include <fstream>

using namespace std; 


/*! \file Mainfunctions.h
    \brief Library of functions used by OpenGl.
    
    This library was created in order to keep the main.cpp file ordered and easy to read
*/

static const int SPACEBAR=32;
static const int ESC=27;

void changeSize(int w, int h);  					//!< Keeps the ratio of the screen if the window changes size
void processNormalKeys(unsigned char key, int xcoor, int ycoor);	//!<Function that reads regular keys from keyboard
void processSpecialKeys( int key, int x, int y);			//!<Function that reads special keys from keyboard
void init(bool arcade, int difficulty);					//!<Initializes a vector of Falling Objects and sets the difficulty level depending on the game mode and settings chosen
void initial();								//!<Asks user for his/her name
void writeScore();							//!<Appends last score to the record, creating a file if needed
void save();								//!<Creates a savefile and stores all the important parameters
void load();								//!<Reads the previously mentioned savefile and sets its values to the affected parameters
void juego();								//!<It is the main display function, the majority of the instructions and limits are here
void mouse(int, int, int, int);						//!<Mouse function used to interact with the menu options
void mousePassive(int, int);						//!<Mouse function to select buttons when hovering over them
void destructor(); 							//!<Destructor function used to delete all pointers created

#endif //MAIN_FUNCTIONS_H
