#include "Player.h"

using namespace std;

Player::Player():Graphics()
{
	points=0;
	vida=10;
	borde=vida;
	off = 0.1;
	fuel=10;

	lifeh=4.5;
	lifew1=3;  //box height 0.5
	lifew2=2.5;
	
	fuelh=4.5;
	fuelw1=1.6;
	fuelw2=1.1;
	flash=false;

	lado=0.5f;

	name="";
}

float  Player ::getSide()
{
	return lado; 
}


void Player :: setLife(float life)
{
	vida=life;
}

void Player :: setFuel(float carbur)
{
	fuel=carbur;
}

void Player :: setPoints(float star)
{
	points=star ;
}

void Player :: changeLife (float change) 
{
	vida+=change; 
	if(vida>10)	
		vida=10; 
	if (vida <0)
		vida=0; 
}

void Player :: changePoints (float change)
{
	points+= change;

}

int Player :: getPoints()
{
	return points;

}

int Player :: getLife()
{
	return vida; 
}

int Player :: getFuel()
{
	return fuel; 
}

void Player :: changeFuel (float change) 
{
	fuel+=change; 
	if(fuel>10)	
		fuel=10; 
	if (fuel <0)
		fuel=0;
}


void Player::display(){
	
glPushMatrix();
	
	glTranslatef(xcoor, ycoor, 0.0f);
	//glColor3f(1.0f,0.0f,0.0f);
	textures("../img/x_wing.bmp", 32, 32,1,1,0.55);
	
glPushMatrix();
	
	//glTranslatef(0.2,4,0);
	
	if(vida>7) glColor3f(0,1,0);
	if(vida<=7 && vida>4) glColor3f(0.91,0.38,0);
	if(vida<=4 && vida >2) glColor3f(1,0,0);	
	if(vida<=2){ 
		if(flash){
			glColor3f(1,0,0); 
			flash=false;
			usleep(1000);
			
		}else{
			glColor3f(0,0,0);
			flash=true;
			usleep(1000);
		}
	}
	//Barra Vida
	glBegin( GL_QUADS );
	glVertex2f(lifeh          , lifew1);
	glVertex2f(lifeh + vida/5 , lifew1);
	glVertex2f(lifeh + vida/5 , lifew2);
	glVertex2f(lifeh          , lifew2);
	glEnd();

glPushMatrix();

	glColor3f(1, 1, 1);	//Borde barra vida
	glBegin( GL_LINE_LOOP );
	glVertex2f(lifeh -off           , lifew1 +off);
	glVertex2f(lifeh + borde/5 +off , lifew1 +off);
	glVertex2f(lifeh + borde/5 +off , lifew2 -off);
	glVertex2f(lifeh -off           , lifew2 -off);
	glEnd();

glPushMatrix();



	//Barra Fuel
	glColor3f(0,0,1);
	glBegin( GL_QUADS );
	glVertex2f(lifeh          , fuelw1);
	glVertex2f(lifeh + fuel/5 , fuelw1);
	glVertex2f(lifeh + fuel/5 , fuelw2);
	glVertex2f(lifeh          , fuelw2);
	glEnd();

glPushMatrix();

	glColor3f(1, 1, 1);	//Borde barra Fuel
	glBegin( GL_LINE_LOOP );
	glVertex2f(lifeh -off           , fuelw1 +off);
	glVertex2f(lifeh + borde/5 +off , fuelw1 +off);
	glVertex2f(lifeh + borde/5 +off , fuelw2 -off);
	glVertex2f(lifeh -off           , fuelw2 -off);
	glEnd();
	
glPushMatrix();

	//Player's name : 
	glPushMatrix();
	glTranslatef(-screenx-2.2, 3-0.5, 0.0f);
	textures("../img/player.bmp", 256, 64,1.8,1,0);

	glPushMatrix();
	char *dummy = strdup(name.c_str());
	printtext(-screenx-2.3, 3.5-1.5,dummy); 
	

	//Points: 
	glPushMatrix(); 
	char point[12]; 
	sprintf(point, "%d", points);

	glPushMatrix();
	glTranslatef(+screenx+1.9, 3-2.6, 0.0f);
	textures("../img/points.bmp", 256, 128,1.8,1,0);

	//printtext(, ,"Points: "); 
	printtext(+screenx+1.8, 2.5-2.8,point); 

	glPopMatrix();

}

void Player:: setName (string name)
{
	this -> name=name; 
}

string Player:: getName ()
{
	return name; 
}




