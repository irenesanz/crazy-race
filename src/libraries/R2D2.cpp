#include "R2D2.h"

/**********************************************  	R2D2		*************************************************************/
void R2D2 :: display ()
{
	glPushMatrix();
   	glTranslatef(xcoor, ycoor, 0.0f);
		textures( "../img/r2d2R.bmp",32,32,1, 1,0.55); 
	R2D2::moveFig();	
}

void R2D2::collision(Player &player)
{
	float side=player.getSide()/2; 
	
	if( (((player.getX()+side <xcoor+side) && (player.getX()+side >xcoor-side)) || ((player.getX()-side <xcoor+side) && (player.getX()-side > xcoor -side))) && (((player.getY()+side>ycoor-side) &&(player.getY()+side<ycoor+side)) || ((player.getY()-side>ycoor-side)&&(player.getY()-side<ycoor+side))) )
	{		

			player.changeFuel(0.2);	
	}	
	
}
