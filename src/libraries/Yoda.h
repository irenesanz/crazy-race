#ifndef YODA_H
#define YODA_H

#include "F_Objects.h"


using namespace std; 
/**
* @class Yoda
 * \brief Yoda creator
 *
 * Class that implements the virtual functions of F_Objects for the concrete case of a Yoda object
 */
class Yoda : public F_Objects
{
public: 
	Yoda():F_Objects(){}//!<Default Constructor
	virtual void collision (Player &); //!<Virtual function collision. It implements the effect of the collision between a player and a Yoda. 
	virtual void display();  //!<Virtual function that displays the picture of a Yoda. 
 };


#endif // YODA_H

