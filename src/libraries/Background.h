#ifndef BACKGROUND_H
#define BACKGROUND_H

#include <fstream>

#include "Graphics.h"
#include "Timer.h"
#include <unistd.h>


/**
 * @class Background
 * @brief Manages various screens during the game.
*/
/**
* This class displays the following screens :
*	- Presentation
*		- Title 
*		- Introduction	
*		- Menu
*		- Settings
*	- Gameplay
*		- Arcade
*		- Survival
*	- Game Over 
*	- Save screen  
*/


class Background : public Graphics 
{

public: 

	Background();//!<Default Constructor
	float lightsaber; //!< Limits of the game zone
	void title();	//!< Title is shown here
	void introduction(); //!< Brief history of the competition and instructions to survive are offered
	void pregame();	//!< Main menu
	void settings();  //!< Difficulty selection, default difficulty is set to "Easy", an option to load a previously saved game is displayed if a savefile exists	
	void display();	//!< Main background display, appears during the game
	void gameOver();  //!< When dead or out of fuel this screen will show
	void savedScreen();  //!< Will appear if at any time while escaping the game is saved 
	void timerDisplay(float, float); //!< Prints the time elapsed

	float  arcadew, arcadeh;	//!<Size variables for the "Arcade" button
	float survivalw, survivalh;	//!<Size variables for the "Survival" button
	float settingsw, settingsh;	//!<Size variables for the "Settings" button
	float notw,noth;		//!<Size variables for the "Easy" button
	float somewhatw, somewhath;	//!<Size variables for the "Normal" button
	float veryw, veryh;		//!<Size variables for the "Hard" button
	float loadw, loadh;		//!<Size variables for the "Load Game" button
	float titlew, titleh;		//!<Size variables for the "Crazy Races" logo

	int selectscreen; 		//!<Variable to switch between screens

	//!List of available screens in the game
	enum screen{titlescreen,introscreen, initscreen, settingsscreen, arcade, survival, gameover, savedplay}; 

	Timer timer; //!< Timer type object from which the elapsed time is read

private:
	void startTimer(); //!<Function that starts to count the time from the moment the player has chosen a gaming mode (Arcade or Survival) or an existing game is loaded
};

#endif //BACKGROUND_H
