#ifndef F_OBJECTS_H
#define F_OBJECTS_H

#include "Player.h"
#include "Graphics.h"
#include <algorithm>

/**
 * @class F_Objects
 * @brief Abstract class derivated from Graphics that manages the common functions and parameters of the moving objects of the game
*/
/**
*  
*/

using namespace std; 

class F_Objects : public Graphics
{
protected:
	void moveFig(); //!<Function that implements the falling of the objects: it decreases the objects' y coordinate until they reach the bottom of the screen and then it moves them up again.  
	float randNum(const float); //!<Function that returns a decimal number. It takes as argument the limit number for the generated one, which will be between -limit and +limit. 
public:
    /**
     * \brief Default constructor that refers to Graphics' constructor and initializes the values of the x and y coordinates of all the falling objects as random numbers.
     */
	F_Objects();

	virtual void display()=0;//!<Pure virtual function that displays each of the falling objects that appear in the game.
	virtual void collision (Player &) =0; //!<Pure virtual function that will perfom an specific action when the player and a falling object collide.
        void changespeed(float); //!<Function that allows to change the protected value of the speed, used to increase the game's difficulty.
	virtual void setSpeed(float); //!<Virtual Function that allows to set a starting speed.It will be common for all the derivated class except Star, which will have a higher speed. It is used when loading a game. 
	float getSpeed(); //!<Function that allows to get the value of the protected value of the speed.

 };


#endif // F_OBJECTS_H
