#include "Foe.h"

/**********************************************  	FOE		*************************************************************/
void Foe :: display ()
{
	glPushMatrix();
   	glTranslatef(xcoor, ycoor, 0.0f);
	textures( "../img/tie_fighter.bmp",32,32,1,1,0.55); 
	Foe::moveFig();	
}

void Foe::collision(Player &player)
{
	float side=player.getSide()/2; 

	if( (((player.getX()+side <xcoor+side) && (player.getX()+side >xcoor-side)) || ((player.getX()-side <xcoor+side) && (player.getX()-side > xcoor -side))) && (((player.getY()+side>ycoor-side) &&(player.getY()+side<ycoor+side)) || ((player.getY()-side>ycoor-side)&&(player.getY()-side<ycoor+side))) )
	{		
			player.changeLife(-0.2);	
	}	
	
}
