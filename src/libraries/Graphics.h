#ifndef GRAPHICS_H
#define GRAPHICS_H

#include <GL/glut.h>
#include <GL/glu.h>
#include <GL/gl.h>

#include <iostream>
#include <time.h>
#include <string>
#include <vector>
#include <malloc.h>
#include <stdio.h>

using namespace std; 

/**
* @class Graphics
 * \brief Graphics creator
 *
 * Abstract class that provides the basis of all the graphic part of the game
 */

class Graphics 
{
protected: 

    /**
     * \brief Default constructor with the inicialization of the common parameters for all the elements of the game.
     */
	Graphics(); 
	
	void textures (const char*  route, float alto, float ancho, float width, float height, float imgoff);  //!<Textures function that is used to map a photograph to an object. It takes the picture's route, its height and width, the width and heigh of the object to which it is going to be stuck and an horizontal offset used to center the image 

	float  screenx, screeny;	//!<Screen's limits, used to set the values between which the falling objects appear randomly
	float  xcoor, ycoor;		//!<Coordinates used to move objects and player around the screen
	
	
	void printtext(float x, float y, char *word); //!<Function to print text to a given coordinate

public: 
	float despl;			//!<Displacement variable
	virtual void display ()=0; 	//!<Virtual display function, each heir class will implement it as needed 
	float getScreenX();		//!<Gets the screens x value
	float getScreenY(); 		//!<Gets the screens y value
	
	float getX(); 			//!<Gets an object's x position 
	float getY(); 			//!<Gets an object's y position
	void setX(float); 		//!<Sets an object's x position
	void setY(float); 		//!<Sets an object's y position
};
#endif //	GRAPHICS_H
