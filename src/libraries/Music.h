#ifndef MUSIC_H
#define MUSIC_H

#include <iostream>
#include "SDL/SDL.h"
#include "SDL/SDL_mixer.h"

using namespace std;

/**
* @class Music
 * \brief This class implements the music component of the game. The library used for this purpose is SDL.  
 */
class Music{
	public:
		Music(const char* route); //!<Constructor that takes as argument the route of the music file. 
		Mix_Music *music; //!<Pointer of the SDL class Mix_Music which is needed to open and play music files. 
		
		int play(); //!< Function that plays the music file. 
		void close(); //!<Function that stops the music. 

};




#endif 
