#ifndef R2D2_H
#define R2D2_H

#include "F_Objects.h"
#include "Player.h"

using namespace std; 

/**
* @class R2D2
 * \brief R2D2 creator
 *
 * Class that implements the virtual functions of F_Objects for the concrete case of a R2D2 object
 */
class R2D2 : public F_Objects
{
public: 
	R2D2() : F_Objects(){}//!<Default Constructor
	virtual void collision (Player &); //!<Virtual function collision. It implements the effect of the collision between a player and a R2D2. 
	virtual void display();  //!<Virtual function that displays the picture of a R2D2. 

 };


#endif // R2D2_H
