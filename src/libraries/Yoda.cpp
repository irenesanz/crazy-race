#include "Yoda.h"

/**********************************************  	YODA		*************************************************************/
void Yoda :: display ()
{
	glPushMatrix();
   	glTranslatef(xcoor, ycoor, 0.0f);
	textures( "../img/yoda.bmp",32,32,1,1,0.55); 
	Yoda::moveFig();	
}

void Yoda::collision(Player &player)
{
	float side=player.getSide()/2; 
	
	if( (((player.getX()+side <xcoor+side) && (player.getX()+side >xcoor-side)) || ((player.getX()-side <xcoor+side) && (player.getX()-side > xcoor -side))) && (((player.getY()+side>ycoor-side) &&(player.getY()+side<ycoor+side)) || ((player.getY()-side>ycoor-side)&&(player.getY()-side<ycoor+side))) )
	{		
			player.changeLife(0.1);	
	}	
	
}

