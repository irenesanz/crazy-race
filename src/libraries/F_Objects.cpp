#include "F_Objects.h"

using namespace std;

F_Objects::F_Objects() : Graphics()
{
	this-> xcoor=randNum(screenx); 
	this->ycoor=randNum(screeny)+10; 
}

void F_Objects:: setSpeed (float speed)
{
	this -> despl=speed; 
}

float F_Objects::getSpeed()
{
	return despl; 
}

void F_Objects::changespeed(float dummy)
{
	despl+=dummy; 
	if (despl >0.6)
		despl=0.6; 
}

float F_Objects::randNum(const float lim)
{
	vector<float> random; 

	for (float i=-lim; i<lim; i+=1)
		random.push_back(i); 

	random_shuffle( random.begin(), random.end() );
	return random[0]; 
}



void F_Objects::moveFig()
{
	if (ycoor<-screeny ){
		ycoor=screeny+randNum(10);

		if (ycoor < screeny){
			ycoor=screeny; 
		}
		xcoor=randNum(screenx);
	}

	ycoor-=despl;
    usleep(1800);
}
