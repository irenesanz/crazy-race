#include "Star.h"

/**********************************************  	Star		*************************************************************/
void Star :: display ()
{
	glPushMatrix();
   	glTranslatef(xcoor, ycoor, 0.0f);
		textures( "../img/star2.bmp",32,32,0.5, 0.5,0.55); 
	Star::moveFig();	
}

void Star ::collision(Player &player)
{
	float side=player.getSide()/2; 
	
	if( (((player.getX()+side <xcoor+side) && (player.getX()+side >xcoor-side)) || ((player.getX()-side <xcoor+side) && (player.getX()-side > xcoor -side))) && (((player.getY()+side>ycoor-side) &&(player.getY()+side<ycoor+side)) || ((player.getY()-side>ycoor-side)&&(player.getY()-side<ycoor+side))) )
	{		

			player.changePoints(1);
	}		
}

void Star:: setSpeed (float speed)
{
	this -> despl=speed+0.2; 
}
