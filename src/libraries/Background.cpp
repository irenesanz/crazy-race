#include "Background.h"	


using namespace std; 

Background :: Background() : Graphics()
{
	
	arcadew=3; 
	arcadeh=1.5;

	survivalw=3;
	survivalh=1.5;

	settingsw=2;
	settingsh=0.5;

	notw=2.5;
	noth=1;

	somewhatw=2.5;
	somewhath=1;

	veryw=2.5;
	veryh=1;
	
	loadw=3;
	loadh=1.5;

	titlew=6;
	titleh=3;

	selectscreen=0; 
}

void Background :: pregame(){
	
	glClearColor(0.0f,0.0f,0.0f, 1.0);
	
	glPushMatrix();

	glTranslatef(-0.25, 2, 0.0f);
	textures("../img/gameplay.bmp", 256,32,7, 1.5,0.05);

	glPushMatrix();

	glTranslatef(-2.5, -1.5, 0.0f);
	textures("../img/arcade.bmp", 128,32, arcadew , arcadeh ,0.16); 

	glPushMatrix();

	glTranslatef(3, -1.5, 0.0f);
	textures("../img/survival.bmp", 128,32, survivalw, survivalh ,0.16);

	glPushMatrix();
	
	glTranslatef(0, -3, 0.0f);
	textures("../img/settings.bmp", 128, 32, settingsw, settingsh ,0.10);

	glPushMatrix();	
}
void Background::startTimer()
{
	static int dummychange=0;
	if (dummychange!= arcade && dummychange!=survival)
		timer.start(); 
	dummychange=selectscreen;
}


void Background:: title()
{	
	glClearColor(0.0f,0.0f,0.0f, 1.0);
	glPushMatrix();
	glTranslatef(-0.5, 0, 0.0f);
		textures("../img/crazyraces.bmp",  256,128, titlew, titleh ,0);
	titlew+=0.01;
	titleh+=0.005;	

	usleep(10000);	

	if(titlew>11.5) selectscreen++;
	glPopMatrix();

}

void Background:: introduction()
{
	glClearColor(0.0f,0.0f,0.0f, 1.0);
	glPushMatrix();
	glTranslatef(-0.5, 0, 0.0f);
	textures("../img/intro.bmp",  512,512, 10,8 ,0);

	glPushMatrix();
	
}


void Background :: display()
{ 

	startTimer();

	lightsaber=4;	

        glClearColor(0.0f,0.0f,0.0f, 1.0);
	glColor3f(1.0f,1.0f,1.0f);

	glPushMatrix();
	
	 // stars:
	textures("../img/star.bmp", 512,512, 14, 9 ,0);
	glPushMatrix();

	// left limit
	glTranslatef(-lightsaber, 0, 0.0f);
	textures("../img/azul.bmp", 32,256,0.5, 8.5,0.55);	

	glPushMatrix();	
 	
	//right limit
	glTranslatef(lightsaber, 0, 0.0f);
	textures("../img/azul.bmp", 32,256,0.5, 8.5,0.55);	

	glPushMatrix();
	
	 // LIFE:
	glTranslatef(5, 3.5, 0.0f);
	textures("../img/life.bmp", 64,32, 1, 0.5 ,0.26);	
	glPushMatrix();

	//FUEL:
	glTranslatef(5, 2, 0.0f);
	textures("../img/fuel.bmp", 64,32, 1, 0.5 ,0.26);	
	glPushMatrix();


	//PLAYMODE:
	glTranslatef(-screenx-2, screeny-1, 0.0f);
	if(selectscreen==arcade)
		textures("../img/arcade.bmp", 128,32, 2.2, 1.2 ,0.16); 
	if(selectscreen==survival)
		textures("../img/survival.bmp", 128,32, 2.2, 1.2 ,0.16);
	glPushMatrix();

	// Time:		
	glPushMatrix();
	Background::timerDisplay(screenx+2,-1);

	glPopMatrix();
}


void Background:: timerDisplay(float x, float y)
{
	char time[12]; 
	sprintf(time, "%d", timer.getTime());
	glTranslatef(x, y, 0.0f);
	textures("../img/time.bmp", 64,16, 1, 0.5 ,0.28);
	printtext(x-0.2,y-0.7,time); 

	
	glPushMatrix();


}

void Background :: savedScreen(){

	glClearColor(0.0f,0.0f,0.0f, 1.0);
	//glColor3f(0.0f,1.0f,0.0f);

	glPushMatrix();

	//left limit
	glTranslatef(-lightsaber, 0, 0.0f);
	textures("../img/green.bmp", 32,256,0.5, 8.5,0.55);	

	glPushMatrix();	
 	
	//right limit
	glTranslatef(lightsaber, 0, 0.0f);
	textures("../img/green.bmp", 32,256,0.5, 8.5,0.55);	
	
	glPushMatrix();
	glTranslatef(0, 0, 0.0f);
	textures("../img/game_saved.bmp", 256,64,6, 1.5,0.1);

	

	glPushMatrix();
	glTranslatef(0, -3.5, 0.0f);
	textures("../img/esc.bmp", 256,32,6, 0.5,0.1);
	
	glPushMatrix();


}

void Background :: gameOver(){

	glClearColor(0.0f,0.0f,0.0f, 1.0);
	//glColor3f(0.0f,1.0f,0.0f);

	glPushMatrix();

	//left limit
	glTranslatef(-lightsaber, 0, 0.0f);
	textures("../img/rojo.bmp", 32,256,0.5, 8.5,0.55);	

	glPushMatrix();	
 	
	//right limit
	glTranslatef(lightsaber, 0, 0.0f);
	textures("../img/rojo.bmp", 32,256,0.5, 8.5,0.55);	

	
	glPushMatrix();
	glTranslatef(-0.3, 0, 0.0f);
	textures("../img/game_over.bmp", 256,128,6, 3,0);
	

	glPushMatrix();
	glTranslatef(0, -3.5, 0.0f);
	textures("../img/esc.bmp", 256,32,6, 0.5,0.1);	
}

void Background :: settings(){
	
	glPushMatrix();

	glTranslatef(0.25, 2, 0.0f);
	textures("../img/awesomeness.bmp", 256,32,8,1,0.07);
	
	glPushMatrix();

	glTranslatef(-3.5, 0.5, 0.0f);
	textures("../img/not.bmp", 128,32,notw,noth,0.10);

	glPushMatrix();

	glTranslatef(0, 0.5, 0.0f);
	textures("../img/somewhat.bmp", 128,32,somewhatw, somewhath,0.10);

	glPushMatrix();

	glTranslatef(3.5, 0.5, 0.0f);
	textures("../img/very.bmp", 128,32,veryw, veryh,0.10);

	glPushMatrix();

	ifstream load("../src/SaveFile.txt");
	if(load.good()){
	glTranslatef(0, -2, 0.0f);
	textures("../img/load.bmp", 128,32,loadw, loadh,0.10);
	}

	glPushMatrix();

}
