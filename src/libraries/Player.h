#ifndef PLAYER_H
#define PLAYER_H

#include "Graphics.h"
#include <string.h>
#include <unistd.h>
/**
* @class Player
 * \brief Player creator
 *
 * Class that implements the parameters and functions used by the player
 */

class Player : public Graphics
{
public:
  

   
	Player();//!<Default Constructor
	void display();//!<Function that displays the player and its parameters: fuel, life and name

	void changePoints(float);//!<Function used to change the private variable of Points
	void changeLife(float); //!<Function used to change the private variable of Life
	void changeFuel(float);//!<Function used to change the private variable of Fuel 
	void setPoints(float);//!<Function used to set an initial value of the private variable of Points. Used when loading the game.
	void setLife(float);//!<Function used to set an initial value of the private variable of Life. Used when loading the game.
	void setFuel(float);//!<Function used to set an initial value of the private variable of Fuel. Used when loading the game.
	void setName(string); //!<Function used to set the name of the player. 
	float getSide();//!<Gets side value	
	
	float lado;	//!<Side of each object
	int getPoints();//!<Function that returns the value of the private variable Points.	
	int getLife(); //!<Function that returns the value of the private variable Life.
	int getFuel(); //!<Function that returns the value of the private variable Fuel.
	string getName(); //!<Function that returns the value of the private variable Name.



private:

	bool flash;	//!<Boolean variable that turns on and off the life bar to make it flash when life is low
	int off, borde; //!<Life and fuel bars' offset (the border of the rectangles)
	float lifew1, lifew2, lifeh; //!<Life bar's top and low y coordinates and left x coordinate
	float fuelw1, fuelw2, fuelh; //!<Fuel bar's top and low y coordinates and left x coordinate
	float vida, fuel; //!<Private parameters of life and fuel.
	int points;//!<Private parameters of points.
	string name; //!<The player's name
 };

#endif // PLAYER_H
