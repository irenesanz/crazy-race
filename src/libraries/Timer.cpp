#include "Timer.h"

Timer:: Timer()
{
	time_start=0;
}
void Timer :: start()
{
	first=time (NULL); 
}	

void Timer :: end()
{
	second=time(NULL); 
}

time_t Timer :: difftime(time_t first, time_t last)
{	
	time_t result=last-first+time_start; 
	return result; 
}

int Timer:: getTime()
{
	Timer::end(); 
	return (int)difftime (first, second); 
}

void Timer :: setInitTime(int time_start)
{
	this-> time_start=time_start; 
}
