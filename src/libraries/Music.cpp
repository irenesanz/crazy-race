#include "Music.h"

Music :: Music(const char* route)
{
        //The music that will be played
        music= NULL;

        //Initialize all SDL subsystems
        if( SDL_Init( SDL_INIT_EVERYTHING ) == -1 )
            {
                cerr<< "SDL_INIT_EVERYTHING not working"<<endl;
            }

        //Initialize SDL_mixer
        if( Mix_OpenAudio( 22050, MIX_DEFAULT_FORMAT, 2, 4096 ) == -1 )
            {
                 cerr<< "MIX_DEFAULT_FORMAT not working"<<endl;
            }
	
	 //Load the music
        music = Mix_LoadMUS( route );    // COMPROBAR QUE SE PASA CORRECTAMENE EL STRING CON LA RUTA DEL MP3

}


int Music:: play(){
    
        //Play the music
         if( Mix_PlayMusic( music, -1 ) == -1 )
            {
                return 1;
            }

}




void Music:: close(){
		
               Mix_HaltMusic();
               Mix_FreeMusic( music );

               //Quit SDL_mixer
               Mix_CloseAudio();
}

