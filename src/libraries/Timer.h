#ifndef TIME_H
#define TIME_H

#include <time.h>
#include <iostream>

/**
 * @class Timer	
 * @brief Manage the timer used in the game.
*/

class Timer 
{
   public: 
    /**
     * \brief Default Constructor with the initialization of the internal parameters used. 
     */
	Timer();
//! Public Function that allows to set the initial time of the timer. Used when loading a game. 
	void setInitTime(int); 
//!Function that starts the counting of the timer. 
	void start(); 
//! Public Function that allows to get the time of the timer. Used to display it on the screen. 
	int getTime(); 

   private: 
//!Private Function that ends the counting of the timer. It is used for the difftime function.
	void end(); 
//!Private Function that gets the time between to values. 
	time_t difftime(time_t, time_t); 
//!Private integer number that stores the initial value of the timer. Used when loading a game. 
	int time_start; 
//! Private numbers that store the initial time and the final time of the timer. 
	time_t first, second; 
};
#endif //TIME_H
