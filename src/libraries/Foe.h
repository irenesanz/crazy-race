#ifndef FOE_H
#define FOE_H

#include "F_Objects.h"


using namespace std; 

/**
* @class Foe
 * \brief Foe creator
 *
 * Class that implements the virtual functions of F_Objects for the concrete case of a Foe object
 */

class Foe : public F_Objects
{
public: 
	Foe():F_Objects(){} //!<Default Constructor
	virtual void collision (Player&); //!<Virtual function collision. It implements the effect of the collision between a player and a foe. 
	virtual void display(); //!<Virtual function that displays the picture of a Foe. 

 };


#endif // FOE_H
