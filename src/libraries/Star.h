#ifndef STAR_H
#define STAR_H

#include "F_Objects.h"
#include "Player.h"

using namespace std; 

/**
* @class Star
 * \brief Star creator
 *
 * Class that implements the virtual functions of F_Objects for the concrete case of a Star object
 */
class Star : public F_Objects
{
public: 
	Star():F_Objects(){ despl=0.3f;}//!<Default Constructor that initializes the moving variable to a higher value than the rest of falling objects. 
	virtual void collision (Player &); //!<Virtual function collision. It implements the effect of the collision between a player and a Star. 
	virtual void display();  //!<Virtual function that displays the picture of a Star. 
	virtual void setSpeed(float); //!<Virtual Function that sets the speed of the Stars when loading a game. 

 };


#endif // STAR_H
