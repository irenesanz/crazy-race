var searchData=
[
  ['save',['save',['../Mainfunctions_8h.html#aae2c382151ef7c9aa913361172b30db6',1,'Mainfunctions.cpp']]],
  ['savedscreen',['savedScreen',['../classBackground.html#a025525a723f39e5f393ca7e1edc3ff74',1,'Background']]],
  ['setfuel',['setFuel',['../classPlayer.html#a3c4b7c02816b045bd3c5a14a20aea20d',1,'Player']]],
  ['setinittime',['setInitTime',['../classTimer.html#a9bfb541e94e8d8482268deaf35f86bce',1,'Timer']]],
  ['setlife',['setLife',['../classPlayer.html#a338dd870dcab71e3d46f450e8307a03c',1,'Player']]],
  ['setname',['setName',['../classPlayer.html#a8eaf43a2f2236b21d0101270ecca1483',1,'Player']]],
  ['setpoints',['setPoints',['../classPlayer.html#a91c78a1f475bd92e37fb38a284e3cbdc',1,'Player']]],
  ['setspeed',['setSpeed',['../classF__Objects.html#ac0e0028e6ac13fe6eb90fc2b82970bca',1,'F_Objects::setSpeed()'],['../classStar.html#a15b4a5d252ba494a0ca1800cefa93b8b',1,'Star::setSpeed()']]],
  ['settings',['settings',['../classBackground.html#a0e608e72f6e0be0755937d0c3f19499d',1,'Background']]],
  ['setx',['setX',['../classGraphics.html#a2fdad9abeb429b75e8d4b8cc603e0fee',1,'Graphics']]],
  ['sety',['setY',['../classGraphics.html#a6e4e98af8368534e8b284e0c7e9aa032',1,'Graphics']]],
  ['star',['Star',['../classStar.html#ac174c691c269196c47324a13c3c9f903',1,'Star']]],
  ['start',['start',['../classTimer.html#a3a8b5272198d029779dc9302a54305a8',1,'Timer']]],
  ['starttimer',['startTimer',['../classBackground.html#aa0518f850c98f3fad9e811fbbfa66be9',1,'Background']]]
];
