CRAZY RACE
===================================================================

![Alt text](https://bytebucket.org/irenesanz/crazy-race/raw/b67ea4dc4afc168b468fed2249b5a4f0e778c25e/img/crazyraces.bmp)


Authors:

 *  [Álvaro Ferrán Cifuentes](http://www.alvaroferran.com)
 *  [Irene Sanz Nieto](http://www.irenesanz.com)
 
 Index
-------------------------------------------------------------------
 * 1.Introduction
 * 2.Compile and run
     * 2.1. Dependencies
     * 2.2. Compiling
     * 2.3. Doxygen documentation
     * 2.4. More info

1. Introduction 
------------------------------------------------------------------
Crazy Race is a game developed for the Computing Systems I subject. 
It has two different play modes (arcade and survival). 
The aim of the game is to stay alive as much time as possible. The rules are explained inside the game: 

![Alt text](https://bytebucket.org/irenesanz/crazy-race/raw/d3bdb95841d078c604b3f82e613281146d516842/img/intro.bmp)


2. Compile and run 
-------------------------------------------------------------------

###2.1 Dependencies###
-----------------------------------------------------------------

It is written in C++ using the libraries SDL (Simple DirectMedia Layer) and OpenGL. 

In order to install SDL, please introduce the following command in the terminal: 
	
    $ sudo apt-get install libsdl1.2-dev libsdl-image1.2-dev libsdl-mixer1.2-dev libsdl-ttf2.0-dev libxmu-dev
 	
In order to install OpenGL, please introduce the following command in the terminal: 

	$ sudo apt-get install freeglut3 freeglut3-dev
	
	
	
###2.2 Compiling###
-----------------------------------------------------------------
This project uses CMAKE for compiling. It has been developed in Ubuntu and has calls to native functions, hence it is not possible to compile it in any other OS. To do so open a terminal and type:
```
#!c++
cmake . 
make
cd bin
./crazyrace

```


If you want to compile it using the terminal and g++, please introduce the following flags: 

	-lglut -lGLU -lGL -lSDL -lSDL_mixer


###2.3 Doxygen documentation###
-----------------------------------------------------------------
* [Latex documentation](https://bytebucket.org/irenesanz/crazy-race/raw/d3c5b68e5bda05dca86267e3c38955830d12bb05/doc/FinalDocument.pdf)

* [HTML documentation](http://irenesanz.bitbucket.org/crazy_race/index.html)

	
###2.4 More info###
-----------------------------------------------------------------
If Latex is throwing the following error: 
 " Metric (TFM) file not found "
 when compiling the Doxygen documentation, please install the following packages using the terminal:
	
    sudo apt-get install  texlive-fonts-extra texlive-fonts-recommended